import time

import pyautogui
import rotatescreen
from pynput.keyboard import Key, Listener

xray_enabled = False
particles_enabled = True
water_enabled = True
highlight_resources_enabled = False
resources_enabled = True

console_key = '`'
xray_key = Key.f4
particles_key = Key.f5
water_key = Key.f6
highlight_resources_key = Key.f7
resources_key = Key.f8


def toggle_xray():
    pyautogui.press(console_key)
    time.sleep(0.3)
    if xray_enabled:
        pyautogui.typewrite('fov 100')
    else:
        pyautogui.typewrite('fov 260')
    time.sleep(0.1)
    pyautogui.press('enter')


def toggle_particles():
    pyautogui.press(console_key)
    time.sleep(0.3)
    if particles_enabled:
        pyautogui.typewrite('')
    else:
        pyautogui.typewrite('')
    time.sleep(0.1)
    pyautogui.press('enter')


def toggle_highlight_resources():
    pyautogui.press(console_key)
    time.sleep(0.3)
    if highlight_resources_enabled:
        pyautogui.typewrite('r.MaterialQualityLevel 1')
    else:
        pyautogui.typewrite('r.MaterialQualityLevel 0')
    time.sleep(0.1)
    pyautogui.press('enter')


def toggle_water():
    pyautogui.press(console_key)
    time.sleep(0.3)
    if water_enabled:
        pyautogui.typewrite('r.Water.SingleLayer 0')
    else:
        pyautogui.typewrite('r.Water.SingleLayer 1')
    time.sleep(0.1)
    pyautogui.press('enter')


def toggle_resources():
    pyautogui.press(console_key)
    time.sleep(0.3)
    if resources_enabled:
        pyautogui.typewrite('wp.Runtime.UpdateStreamingSources 0')
    else:
        pyautogui.typewrite('wp.Runtime.UpdateStreamingSources 1')
    time.sleep(0.1)
    pyautogui.press('enter')


def flip_screen():
    screen = rotatescreen.get_primary_display()
    if xray_enabled:
        screen.set_landscape_flipped()
        return
    screen.set_landscape()


def on_press(key):
    return


def on_release(key):
    if key == Key.esc:
        # Stop listener
        exit()
    if key == xray_key:
        global xray_enabled
        toggle_xray()
        flip_screen()
        xray_enabled = not xray_enabled
    if key == particles_key:
        global particles_enabled
        toggle_particles()
        particles_enabled = not particles_enabled
    if key == water_key:
        global water_enabled
        toggle_water()
        water_enabled = not water_enabled
    if key == highlight_resources_key:
        global highlight_resources_enabled
        toggle_highlight_resources()
        highlight_resources_enabled = not highlight_resources_enabled
    if key == resources_key:
        global resources_enabled
        toggle_resources()
        resources_enabled = not resources_enabled


def main():
    print(f"{xray_key} to toggle xray\n"
          f"{particles_key} to toggle particles\n"
          f"{water_key} to toggle water\n"
          f"{highlight_resources_key} to toggle highlight resources\n"
          f"{resources_key} to toggle resources\n")
    while True:
        with Listener(
                on_press=on_press,
                on_release=on_release) as listener:
            listener.join()
